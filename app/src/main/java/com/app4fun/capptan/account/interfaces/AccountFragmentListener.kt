package com.app4fun.capptan.account.interfaces

interface AccountFragmentListener {

    fun onNewAccount()
}