package com.app4fun.capptan.account.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.app4fun.capptan.R
import com.app4fun.capptan.account.interfaces.AccountView
import com.app4fun.capptan.account.presenter.AccountPresenter
import com.app4fun.capptan.infrastructure.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_new_account.*

class NewAccountFragment : BaseFragment(), AccountView {

    private lateinit var accountPresenter: AccountPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        accountPresenter = AccountPresenter(this, requireContext())

        button_sign_up.setOnClickListener {
            val name = edit_name.text.toString()
            val email = edit_email.text.toString()
            val password = edit_password.text.toString()
            accountPresenter.createAccount(email, password, name)
        }
    }

    override fun authSuccess() {
        Toast.makeText(requireContext(), "Conta criada com sucesso", Toast.LENGTH_SHORT).show()
    }

    override fun onFailure(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }
}
