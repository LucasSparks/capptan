package com.app4fun.capptan.account.presenter

import android.content.Context
import com.app4fun.capptan.R
import com.app4fun.capptan.account.interfaces.AccountView
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.*

class AccountPresenter (
    var view: AccountView?,
    private var context: Context
) {

    fun auth (email: String, password: String) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                view?.authSuccess()
            }.addOnFailureListener{
                verifyFirebaseException(FirebaseExceptionState.LOGGING_IN, it)
            }
    }

    fun createAccount (email: String, password: String, fullName: String) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                updateUserProfile(fullName)
            }.addOnFailureListener {
                verifyFirebaseException(FirebaseExceptionState.CREATING_ACCOUNT, it)
            }
    }

    fun logOut () {
        FirebaseAuth.getInstance().signOut()
    }

    private fun updateUserProfile(fullName: String) {
        FirebaseAuth.getInstance().currentUser?.updateProfile(
            UserProfileChangeRequest.Builder()
                .setDisplayName(fullName)
                .build()
        )
    }

    /**
     * Mapping erros and translating message of Firebase to Portuguese
     * @param case
     * @param e
     */
    private fun verifyFirebaseException(case: FirebaseExceptionState, e: Exception) {
        val messageException: String = when (e) {
            is FirebaseAuthInvalidUserException -> context.getString(R.string.firebase_message_invalid_user)
            is FirebaseAuthUserCollisionException -> context.getString(R.string.firebase_message_account_collision)
            is FirebaseNetworkException -> context.getString(R.string.firebase_message_error_network)
            is FirebaseAuthInvalidCredentialsException -> context.getString(R.string.firebase_message_invalid_credentials)
            is FirebaseAuthWeakPasswordException -> context.getString(R.string.firebase_message_weak_password)
            is IllegalArgumentException -> context.getString(R.string.firebase_message_illegal_exception)
            else -> when (case) {
                FirebaseExceptionState.CREATING_ACCOUNT -> context.getString(R.string.firebase_message_auth_error)
                FirebaseExceptionState.LOGGING_IN -> context.getString(R.string.firebase_message_sign_in_error)
                else -> context.getString(R.string.firebase_message_reset_error)
            }
        }
        view?.onFailure(messageException)
    }

    enum class FirebaseExceptionState { CREATING_ACCOUNT, LOGGING_IN, RESETTING_PASSWORD }
}