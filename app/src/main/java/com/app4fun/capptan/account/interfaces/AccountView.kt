package com.app4fun.capptan.account.interfaces

interface AccountView {
    fun authSuccess()
    fun onFailure(message: String)
}