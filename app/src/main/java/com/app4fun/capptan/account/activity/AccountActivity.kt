package com.app4fun.capptan.account.activity

import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import com.app4fun.capptan.R
import com.app4fun.capptan.account.fragments.LoginFragment
import com.app4fun.capptan.account.fragments.NewAccountFragment
import com.app4fun.capptan.account.interfaces.AccountFragmentListener
import com.app4fun.capptan.infrastructure.base.BaseActivity


class AccountActivity : BaseActivity(), AccountFragmentListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.app4fun.capptan.R.layout.activity_account)

        replaceFragment(LoginFragment(), R.id.container)

        // In Activity's onCreate() for instance
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = window
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }
    }

    override fun onNewAccount() {
        replaceFragment(NewAccountFragment(), R.id.container)
    }
}
