package com.app4fun.capptan.account.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.app4fun.capptan.R
import com.app4fun.capptan.account.interfaces.AccountFragmentListener
import com.app4fun.capptan.account.interfaces.AccountView
import com.app4fun.capptan.account.presenter.AccountPresenter
import com.app4fun.capptan.infrastructure.base.BaseFragment
import com.app4fun.capptan.infrastructure.extension.attachCallbackTo
import com.app4fun.capptan.infrastructure.extension.setClickTextInfo
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : BaseFragment(), AccountView {

    private var callback: AccountFragmentListener? = null
    private lateinit var accountPresenter: AccountPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        accountPresenter = AccountPresenter(this, requireContext())

        text_new_account.setClickTextInfo("Ainda não é cadastrado?", "Clique aqui.") {
            callback?.onNewAccount()
        }

        button_sign_in.setOnClickListener {
            val login = edit_email.text.toString()
            val password = edit_password.text.toString()
            accountPresenter.auth(login, password)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = attachCallbackTo(context)
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    override fun onFailure(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun authSuccess() {
        Toast.makeText(requireContext(), "Logado com sucesso", Toast.LENGTH_SHORT).show()
    }
}
