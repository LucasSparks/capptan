package com.app4fun.capptan.infrastructure.extension

import android.content.Context
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.app4fun.capptan.R
import com.app4fun.capptan.infrastructure.base.BaseActivity

fun <T : View> ViewGroup.inflate(layoutRes: Int, inflate: ((root: T) -> Unit)? = null): T {
    val view = LayoutInflater.from(context).inflate(layoutRes, this, false) as T
    inflate?.let {
        it(view)
        addView(view)
    }
    return view
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun View.showKeyboard() {
    postDelayed({
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }, 150)
}

fun RecyclerView.addDivider() {
    this.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    this.itemAnimator = DefaultItemAnimator()
}

fun TextView.setClickTextInfo(
        normalText: String, clickText: String,
        click: (() -> Unit)? = null
) {
    val ss = SpannableString(clickText)

    val clickableSpan = object : ClickableSpan() {
        override fun onClick(widget: View?) {
            click?.invoke()
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.color = resources.getColor(R.color.white)
            ds.isUnderlineText = false
        }
    }

    ss.setSpan(clickableSpan, 0, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

    text = TextUtils.concat(normalText, " ", ss)
    movementMethod = LinkMovementMethod.getInstance()
}

fun TextView.setClickTextTwoInfo(
        firstText: String, firstClickText: String, secondText: String, secondClickText: String,
        firstClick: (() -> Unit)? = null, secondClick: (() -> Unit)? = null
) {
    val firstSpannableClick = SpannableString(firstClickText)
    val secondSpannableClick = SpannableString(secondClickText)

    val firstClickableSpan = object : ClickableSpan() {
        override fun onClick(widget: View?) {
            firstClick?.invoke()
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.color = resources.getColor(R.color.colorAccent)
            ds.isUnderlineText = false
        }
    }

    val secondClickableSpan = object : ClickableSpan() {
        override fun onClick(widget: View?) {
            secondClick?.invoke()
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.color = resources.getColor(R.color.colorAccent)
            ds.isUnderlineText = false
        }
    }

    firstSpannableClick.setSpan(
            firstClickableSpan,
            0,
            firstSpannableClick.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    secondSpannableClick.setSpan(
            secondClickableSpan,
            0,
            secondSpannableClick.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )

    text = TextUtils.concat(
            firstText, " ", firstSpannableClick, " ", secondText, " ", secondSpannableClick
    )
    movementMethod = LinkMovementMethod.getInstance()
}


fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) = afterTextChanged(s.toString())
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}