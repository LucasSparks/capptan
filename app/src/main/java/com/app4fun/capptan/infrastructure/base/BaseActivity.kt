package com.app4fun.capptan.infrastructure.base

import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import kotlin.reflect.KClass

open class BaseActivity : AppCompatActivity() {


    fun openActivity(
            activity: KClass<out AppCompatActivity>,
            handleIntent: ((Intent) -> Unit)? = null
    ) {
        val intent = Intent(this, activity.java)
        handleIntent?.invoke(intent)
        startActivity(intent)
    }

    fun openActivityForResult(
            activity: KClass<out AppCompatActivity>,
            requestCode: Int,
            handleIntent: ((Intent) -> Unit)? = null
    ) {
        val intent = Intent(this, activity.java)
        handleIntent?.invoke(intent)
        startActivityForResult(intent, requestCode)
    }

    fun replaceFragment(fragment: Fragment, container: Int, tag: String = "") {
        supportFragmentManager.beginTransaction().apply {
            replace(container, fragment, tag)
            supportFragmentManager.findFragmentById(container)?.let { addToBackStack(null) }
            commit()
        }
    }

    fun popStackAndReplaceFragment(fragment: Fragment, container: Int, tag: String = "") {
        if (supportFragmentManager.findFragmentById(container)?.javaClass == fragment.javaClass) return
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        supportFragmentManager.beginTransaction()
                .replace(container, fragment, tag)
                .commit()
    }

}
