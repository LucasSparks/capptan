package com.app4fun.capptan.infrastructure.extension

inline fun <reified T : Any> attachCallbackTo(obj: Any?): T {
    return try {
        obj as T
    } catch (e: ClassCastException) {
        throw ClassCastException("$obj must implement ${T::class.java.simpleName}")
    }
}