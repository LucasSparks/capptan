package com.app4fun.capptan.infrastructure.base

import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlin.reflect.KClass

open class BaseFragment : Fragment() {

    fun openActivity(
            activity: KClass<out AppCompatActivity>,
            handleIntent: ((Intent) -> Unit)? = null
    ) {
        val intent = Intent(requireContext(), activity.java)
        handleIntent?.invoke(intent)
        startActivity(intent)
    }

    fun openActivityForResult(
            activity: KClass<out AppCompatActivity>,
            requestCode: Int,
            handleIntent: ((Intent) -> Unit)? = null
    ) {
        val intent = Intent(requireContext(), activity.java)
        handleIntent?.invoke(intent)
        startActivityForResult(intent, requestCode)
    }
}